﻿namespace CircularPrime
{
    public class PrimeService
    {
        public int Compute()
        {
            int numberCounter = 0;
            for (int i = 1; i <= 1000000; i++)
            {
                if (isPrime(i) && FindCircularPrimeNumbers(i))
                {
                    //Console.WriteLine(i);
                    numberCounter++;
                }
            }
            return  numberCounter;
        }
        private bool FindCircularPrimeNumbers(int number)
        {
            bool flag = false;
            int rotateResult = number;
            int resultNumber;
            do
            {
                resultNumber = leftRotateShift(rotateResult.ToString(), 1);
                if (!isPrime(resultNumber))
                {
                    return flag;
                }
                else
                {
                    flag = true;
                }
            } while (rotateResult != number);
            return flag;
        }
        private bool isPrime(int number)
        {
            if (number == 1) return false;
            if (number == 2) return true;

            if (number % 2 == 0) return false;

            for (int i = 3; i < number; i += 2)
            {
                if (number % i == 0) return false;
            }
            return true;
        }
        private int leftRotateShift(string key, int shift)
        {
            shift %= key.Length;
            string res = key.Substring(shift) + key.Substring(0, shift);
            int intres = int.Parse(res);
            return intres;
        }
    }
}
