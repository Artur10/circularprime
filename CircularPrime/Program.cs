﻿using System;
using System.Threading.Tasks;

namespace CircularPrime
{
    class Program
    {
        static void Main(string[] args)
        {
            PrimeService ps = new PrimeService();
            Task<int> t = new Task<int>(() => ps.Compute());
            t.Start();
            Console.WriteLine("Please wait.Start computing");
            Task finalTsk = t.ContinueWith(task => Console.WriteLine("Amount of numbers is: " + task.Result));
            Console.ReadLine();  
        }
    }
}
